//1st way with separate functions
const fs = require('fs');


function deleteFile(file, callback) {
    fs.unlink(file, (err) => {
            callback(err);
    });
}
function readingFromFile(file, callback) {
    fs.readFile(file, 'utf-8', (err, data) => {
        callback(err, data);
    });
};

function writingInFile(file, data, callback) {
    fs.writeFile(file, data, (err) => {
        callback(err);
    });
};

function appendInFile(file, data, callback) {
    fs.appendFile(file, data, (err) => {
        callback(err);
    });
};
function createSortAndDeleteFiles(lipsumFilePath) {
    readingFromFile(lipsumFilePath, (err, lipsumData) => {
        if (err) console.error(err);
        else {
            let upperCaseData = lipsumData.toUpperCase();
            writingInFile("upperCaseFile.txt", upperCaseData, (err) => {
                if (err) console.error(err);
                else {
                    writingInFile("filenames.txt", "upperCaseFile.txt\n", (err) => {
                        if (err) console.error();
                        else {
                            let lowerCaseData = upperCaseData.toLowerCase().split(".");
                            let sentenceFile = "sentenceFile.txt";
                            lowerCaseData.forEach((sentence) => {
                                appendInFile(sentenceFile, `${sentence}\n`, (err) => {
                                    if (err) {
                                        console.error(err);
                                        return;
                                    }
                                });
                            });
                            appendInFile("filenames.txt", `${sentenceFile}\n`, (err) => {
                                if (err) {
                                    console.error();
                                    return;
                                }
                            });
                            readingFromFile("filenames.txt", (err, fileListData) => {
                                if (err) console.error(err);
                                else {
                                    fileListData = fileListData.split('\n').filter((file) => file !== '');
                                    fileListData.forEach((fileName) => {
                                        readingFromFile(fileName, (err, eachFileData) => {
                                            if (err) console.error(err);
                                            else {
                                                eachFileData = eachFileData.split(' ');
                                                eachFileData.sort((a, b) => {
                                                    if (a > b) {
                                                        return 1;
                                                    } else if (a < b) {
                                                        return -1;
                                                    }
                                                    else {
                                                        return 0;
                                                    }
                                                });
                                                eachFileData = eachFileData.toString();
                                                sortedFile = `sorted${fileName}`;
                                                writingInFile(sortedFile, eachFileData, (err) => {
                                                    if (err) console.error(err);
                                                    else {
                                                        console.log('new' + 'sorted' + fileName + 'created with sorted data');
                                                        appendInFile('/home/jayesh/fs-Callbacks/test/filenames.txt', `${sortedFile}\n`, (err) => {
                                                            if (err) {
                                                                console.error(err);
                                                                return;
                                                            }
                                                        });
                                                    }
                                                });
                                            };
                                        });
                                    });
                                    fs.readFile("/home/jayesh/fs-Callbacks/test/filenames.txt", 'utf-8',(err, filesNamesContent) => {
                                        if (err) console.error(err);
                                        else {
                                            let files = filesNamesContent.split('\n').filter((file)=> file!="");
                                            files.forEach((file) => {
                                                deleteFile(file, (err) => {
                                                    console.log(file);
                                                    if (err) console.error(err);
                                                    else {
                                                        console.log(file + 'deleted');
                                                    }
                                                });

                                            });
                                        }
                                    });
                                }
                            });
                        };
                    });
                }
            });
        }
    });
}
module.exports = createSortAndDeleteFiles;

//2nd way without separate functions 
// const fs = require('fs');

// function createSortAndDeleteFiles(callback) {
//     fs.readFile("/home/jayesh/fs-Callbacks/lipsum.txt", 'utf-8', (err, lipsumContent) => {
//         if (err) {
//             console.log(err);
//             return;
//         } else {

//             lipsumContent = lipsumContent.toUpperCase();
//             let upperCaseFile = "upperCaseFile.txt";
//             callback(err, upperCaseFile + " created");
//             fs.writeFile(upperCaseFile, lipsumContent, (err) => {
//                 if (err) {
//                     console.log(err);
//                     return;
//                 } else {
//                     callback(err, `Write in ${upperCaseFile}`);
//                     fs.writeFile('./filenames.txt', upperCaseFile, (err) => {  //only uppercaseFile in filenames
//                         if (err) {
//                             console.log(err);
//                             return;
//                         } else {
//                             callback(err, `Inserted filename in filenames.txt`);
//                             fs.readFile(upperCaseFile, 'utf-8', (err, upperCaseData) => {
//                                 if (err) {
//                                     console.log(err);
//                                     return;
//                                 } else {
//                                     callback(err, upperCaseFile + ' created');
//                                     upperCaseData = upperCaseData.toLowerCase();
//                                     upperCaseData = upperCaseData.split('.');
//                                     let sentenceFile = "sentenceFile.txt";
//                                     upperCaseData.forEach(sentence => {
//                                         fs.appendFile(sentenceFile, sentence + '\n', (err) => {
//                                             if (err) {
//                                                 console.log(err);
//                                                 return;
//                                             }
//                                             callback(err, `Adding in sentence file ...`);
//                                         });
//                                     });

//                                     fs.appendFile('./filenames.txt', '\n' + sentenceFile, (err) => {
//                                         if (err) {
//                                             console.log(err);
//                                             return;
//                                         } else {
//                                             callback(err, `Added ${sentenceFile} to filenames.txt`);
//                                             fs.readFile('./filenames.txt', 'utf-8', (err, filesData) => {
//                                                 if (err) {
//                                                     console.log(err)
//                                                     return;
//                                                 } else {
//                                                     callback(err, `files Data are ${filesData}`);
//                                                     filesData = filesData.split('\n');
//                                                     filesData.forEach(file => {
//                                                         fs.readFile(file, 'utf-8', (err, content) => {
//                                                             if (err) {
//                                                                 console.log(err)
//                                                             } else {

//                                                                 content = content.split(' ');
//                                                                 content.sort((a, b) => {
//                                                                     if (a > b) {
//                                                                         return 1;
//                                                                     } else if (a < b) {
//                                                                         return -1;
//                                                                     }
//                                                                     else {
//                                                                         return 0;
//                                                                     }

//                                                                 })
//                                                                 content = content.toString();
//                                                                 sortedFile = 'sorted_' + file;
//                                                                 fs.writeFile(sortedFile, content, (err) => {
//                                                                     if (err) {
//                                                                         console.log(err);
//                                                                         return;
//                                                                     }
//                                                                 })
//                                                                 callback(err, `created ${sortedFile} successfully`);
//                                                                 fs.appendFile('filenames.txt', '\n' + sortedFile, (err) => {
//                                                                     if (err) {
//                                                                         console.log(err);
//                                                                         return;
//                                                                     } else {

//                                                                         fs.readFile('./filenames.txt', 'utf-8', (err, filesData) => {
//                                                                             if (err) {
//                                                                                 console.log('err');
//                                                                                 return;
//                                                                             } else {

//                                                                                 filesData = filesData.split('\n');
//                                                                                 filesData.forEach(file => {
//                                                                                     fs.unlink(file, err => {
//                                                                                         if (err) {
//                                                                                             console.log("file deleted Already");
//                                                                                         } else {
//                                                                                             callback(err, "file deleted successfully");
//                                                                                         }
//                                                                                     })
//                                                                                 })
//                                                                             }
//                                                                         })
//                                                                     }
//                                                                 })
//                                                             }
//                                                         })
//                                                     })
//                                                 }
//                                             })
//                                         }
//                                     })
//                                 }
//                             })
//                         }
//                     })
//                 }
//             })
//         }
//     })
// }

// module.exports = createSortAndDeleteFiles;
