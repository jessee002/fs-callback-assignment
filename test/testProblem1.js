let data = require('../problem1');

let makeDirectory = data['makeDirectory'];
let makeRandomFiles =  data['makeRandomFiles'];
let deleteFiles =  data['deleteFiles'];

let directoryPath= makeDirectory("RandomDirectory");
let makeRandomFilesResults = makeRandomFiles(directoryPath,15);
let deleteFilesResult = deleteFiles(directoryPath);


