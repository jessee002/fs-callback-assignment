const fs = require('fs');
const path = require('path');

function makeDirectory(directoryName) {
    let directoryPath = path.join(__dirname, directoryName);

    fs.mkdir(directoryPath, err => {
        if (err) {
            console.error(err);
        }
    });
    console.log("directory created");
    return directoryPath;
};

function makeRandomFiles(directoryPath, num) {

    for (let idx = 0; idx <= num; idx++) {
        let file = Math.random().toString().slice(2, 4) + '.json';
        let filePath = path.join(directoryPath, file);  

        fs.writeFile(filePath, "Hello jessee", (err) => {
            if (err) {
                console.error(err);
            }
        });
    }
    console.log("random files created");
    return directoryPath;
};

function deleteFiles(directoryPath) {

    fs.readdir(directoryPath, (error, data) => {
        if (error) {
            console.error("some error occured");
        }
        else {
            data.forEach((fileName) => {  
                const filePath = path.join(directoryPath, fileName);

                fs.unlink(filePath, (err) => {
                    if (err) {
                        console.error("error occured while deleting th file");
                        return;
                    }
                });
            });
        };
    });
    console.log("files deleted");
}
module.exports = {makeDirectory,makeRandomFiles,deleteFiles};


